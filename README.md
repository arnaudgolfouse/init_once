A crate to make `static` variables that are initialized once, and then use them
without overhead. That is, the `static` can be read without any runtime checks.

This is achieved by obtaining a type that proves that initialization was done,
and using that type to get references to the `static`.

# Example

```rust
init_once::init_once! { static INIT_ONCE: Vec<i32> => Proof; }

let initialization_proof: INIT_ONCE::Proof = INIT_ONCE::init(vec![1, 2, 3]);

assert!(INIT_ONCE::is_initialized().is_some());
assert_eq!(initialization_proof.get().as_slice(), &[1, 2, 3]);
assert!(INIT_ONCE::try_init(Vec::new()).is_err());
```

This crate is `#[no_std]`.

# Alternatives

A similar crate exists, with a slightly different design: <https://crates.io/crates/init-token>
