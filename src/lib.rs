//! A crate to make `static` variables that are initialized once, and then use them
//! without overhead. That is, the `static` can be read without any runtime checks.
//!
//! This is achieved by obtaining a type that proves that initialization was done,
//! and using that type to get references to the `static`.
//!
//! # Example
//!
//! ```rust
//! init_once::init_once! { static INIT_ONCE: Vec<i32> => Proof; }
//!
//! let initialization_proof: INIT_ONCE::Proof = INIT_ONCE::init(vec![1, 2, 3]);
//!
//! assert!(INIT_ONCE::is_initialized().is_some());
//! assert_eq!(initialization_proof.get().as_slice(), &[1, 2, 3]);
//! assert!(INIT_ONCE::try_init(Vec::new()).is_err());
//! ```
//!
//! This crate is `#[no_std]`.
//!
//! # Alternatives
//!
//! A similar crate exists, with a slightly different design: <https://crates.io/crates/init-token>

#![no_std]

/// Generate a `static` variable that can be initialized once, and then used
/// without any overhead.
///
/// The generated API is roughly the following:
/// ```
/// // result of init_once! { static INIT_ONCE: i32 => Proof; }
/// mod INIT_ONCE {
///     pub struct Proof(/* */);
///
///     impl Proof {
///         pub fn get(self) -> &'static i32 {
///             // ...
/// # todo!()
///         }
///     }
///
///     pub fn init(value: i32) -> Proof {
///         // ...
/// # todo!()
///     }
///
///     pub fn try_init(value: i32) -> Result<Proof, Proof> {
///         // ...
/// # todo!()
///     }
///
///     pub fn is_initialized() -> Option<Proof> {
///         // ...
/// # todo!()
///     }
/// }
/// ```
///
/// # Example
/// ```
/// # use init_once::init_once;
/// init_once! {
///     static INIT_ONCE_1: i32 => Proof;
///     static INIT_ONCE_2: i32 => Proof2;
///     // Proofs can have the same name, because they are namespaced
///     static INIT_ONCE_3: i32 => Proof;
/// }
/// let initialization_proof: INIT_ONCE_1::Proof = INIT_ONCE_1::init(2);
/// assert_eq!(*initialization_proof.get(), 2);
/// ```
#[macro_export]
macro_rules! init_once {
    ($(
        $(#[$attr:meta])* $visibility:vis static $name:ident : $typ:ty => $proof_name:ident;
    )+) => {$(
#[allow(non_snake_case)]
$(#[$attr])*
$visibility mod $name {
    #[allow(unused_imports)]
    use super::*;

    #[allow(non_camel_case_types)]
    struct $name {
        inner: ::core::cell::UnsafeCell<::core::mem::MaybeUninit<$typ>>,
        initialized: ::core::sync::atomic::AtomicBool,
    }
    unsafe impl ::core::marker::Sync for $name {}
    unsafe impl ::core::marker::Send for $name {}
    static INIT_ONCE: $name = $name {
        inner: ::core::cell::UnsafeCell::new(::core::mem::MaybeUninit::<$typ>::uninit()),
        initialized: ::core::sync::atomic::AtomicBool::new(false),
    };

    /// A proof that the global variable has been initialized.
    ///
    /// You must get one via [`init`]. Then, you can copy it as much as you want, and use it in [`get`] to
    /// get an immutable reference to the data.
    ///
    /// This type is zero-sized.
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct $proof_name(());

    impl $proof_name {
        /// Get the inner value of the global variable, given a proof of initialization.
        #[allow(dead_code)]
        pub fn get(self) -> &'static $typ {
            // SAFETY: the `$proof_name` assures us that the value is
            // initialized, and will never be written to again.
            unsafe { &*(&*INIT_ONCE.inner.get()).as_ptr() }
        }
    }

    /// Initialize the global variable with `value`, returning a proof of initialization.
    ///
    /// # Panics
    ///
    /// Panics if the value was already initialized.
    #[allow(dead_code)]
    pub fn init(value: $typ) -> $proof_name {
        if INIT_ONCE
            .initialized
            .swap(true, ::core::sync::atomic::Ordering::SeqCst)
        {
            panic!("Inner value was already initialized!")
        } else {
            // SAFETY:
            // - `_inner::INIT_ONCE.initialized` garantees that no other thread can access this portion of code.
            // - This use of `MaybeUninit` is sound anyways.
            unsafe {
                let mut_maybe_uninit = &mut *INIT_ONCE.inner.get();
                mut_maybe_uninit.as_mut_ptr().write(value);
            }
            $proof_name(())
        }
    }

    /// Initialize the global variable with `value`, returning a proof of initialization.
    ///
    /// Returns `Err` if the value was already initialized.
    #[allow(dead_code)]
    pub fn try_init(value: $typ) -> Result<$proof_name, $proof_name> {
        if INIT_ONCE
            .initialized
            .swap(true, ::core::sync::atomic::Ordering::SeqCst)
        {
            Err($proof_name(()))
        } else {
            // SAFETY:
            // - `_inner::INIT_ONCE.initialized` garantees that no other thread can access this portion of code.
            // - This use of `MaybeUninit` is sound anyways.
            unsafe {
                let mut_maybe_uninit = &mut *INIT_ONCE.inner.get();
                mut_maybe_uninit.as_mut_ptr().write(value);
            }
            Ok($proof_name(()))
        }
    }

    /// Return a proof if the global variable is already initialized. Else, return `None`.
    #[allow(dead_code)]
    pub fn is_initialized() -> Option<$proof_name> {
        if INIT_ONCE
            .initialized
            .load(::core::sync::atomic::Ordering::Acquire)
        {
            Some($proof_name(()))
        } else {
            None
        }
    }
}
    )+};
}

#[cfg(test)]
mod tests {
    extern crate std;
    use std::string::String;

    use super::*;
    use std::{thread, vec::Vec};

    #[test]
    fn synchronous_reads() {
        init_once! { static TEST_VALUE: String => Proof; }

        let proof = TEST_VALUE::init(String::from("hello"));
        for _ in 0..40 {
            assert_eq!(proof.get(), "hello");
        }
    }

    #[test]
    #[should_panic]
    fn fail_synchronous_writes() {
        init_once! { static TEST_VALUE: String => Proof; }

        for _ in 0..2 {
            TEST_VALUE::init(String::from("uh-oh"));
        }
    }

    #[test]
    fn concurrent_reads() {
        init_once! { static TEST_VALUE: String => Proof; }

        let proof = TEST_VALUE::init(String::from("hello"));

        let mut threads = Vec::new();
        for _ in 0..40 {
            threads.push(thread::spawn(move || proof.get()));
        }
        for t in threads {
            assert_eq!(t.join().unwrap(), "hello");
        }
    }

    #[test]
    #[should_panic]
    fn fail_concurrent_writes() {
        init_once! { static TEST_VALUE: String => Proof; }

        let mut threads = Vec::new();
        for _ in 0..2 {
            threads.push(thread::spawn(move || {
                TEST_VALUE::init(String::from("uh-oh"));
            }));
        }
        for t in threads {
            t.join().unwrap();
        }
    }
}
